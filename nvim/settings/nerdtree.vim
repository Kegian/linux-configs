" ================================================================================
"                                  My NeoVIM settings
"                                  Author: Oleg Lelenkov
" ================================================================================

map <C-n> :NERDTreeToggle<CR>

let NERDTreeAutoDeleteBuffer = 1
let NERDTreeMapActivateNode='<space>' 
