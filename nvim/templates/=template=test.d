/**
 * Test module %MODULE%
 *
 * Copyright: %COPY%
 * License: %LICENSE%
 * Author: <%MAIL%> %USER%
 * Date: %DATE%
 */

module %MODULE%_test;

private
{
    import dunit;
}


class Test%CLASS%
{
    mixin UnitTest;
}
