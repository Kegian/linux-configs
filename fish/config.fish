################
# SOURCE LOCAL #
################

if test -e ~/.config/fish/local.config.fish
    source ~/.config/fish/local.config.fish
end

##############
# FISH THEME #
##############

set -g theme_nerd_fonts yes
set -g theme_show_exit_status yes

#########################
# Aliases and functions #
#########################

alias ll "ls -lah"
alias vim "vim.tiny"
alias gr "grep -r"

function mkcd
    mkdir $argv ;and cd $argv
end

###############
# NVIM THINGS #
###############

# set PATH $PATH ~/.pyenv/bin

# pyenv init - | source
# pyenv virtualenv-init - | source

#############
# GO THINGS #
#############

set -x GOPATH ~/go

set PATH $PATH /usr/local/go/bin
set PATH $PATH ~/go/bin

alias mfmt "gofmt -s -w"

###############
# OVER THINGS #
###############

set PATH $PATH ~/bin
set PATH $PATH ~/.local/bin
